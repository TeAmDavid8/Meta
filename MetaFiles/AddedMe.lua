local function AddedMe(msg)
local text = msg.content.text.text
--- Start Code ---
if text and text:match("منو ضافني") then
if not redis:get(idbot..'Rio:Added:Me'..msg.chat_id) then
local StatusMember = LuaTele.getChatMember(msg.chat_id,msg.sender_id.user_id).status.luatele
if (StatusMember == "chatMemberStatusCreator") then
LuaTele.sendText(msg.chat_id,msg.id,"∞︙انت منشئ المجموعه","md") 
return false
end
local Added_Me = redis:get(idbot.."Who:Added:Me"..msg.chat_id..':'..msg.sender_id.user_id)
if Added_Me then 
local result = LuaTele.getUser(Added_Me)
local Name = '['..result.first_name..'](tg://user?id='..result.id..')'
Text = '∞︙*الشخص الذي قام باضافتك هو* ↫ '..Name
LuaTele.sendText(msg.chat_id,msg.id,Text,'md')
else
LuaTele.sendText(msg.chat_id, msg.id, '∞︙انت دخلت عبر الرابط', 'md') 
end
else
LuaTele.sendText(msg.chat_id, msg.id, '∞︙امر منو ضافني تم تعطيله من قبل المدراء', 'md') 
end
end
--- On and Of AddedMe ---
if text == 'تفعيل ضافني' and Manager(msg) then 
redis:del(idbot..'Rio:Added:Me'..msg.chat_id) 
LuaTele.sendText(msg.chat_id, msg.id, '∞︙تم تفعيل امر منو ضافني', 'md') 
end
if text == 'تعطيل ضافني' and Manager(msg) then 
redis:set(idbot..'Rio:Added:Me'..msg.chat_id,true) 
LuaTele.sendText(msg.chat_id, msg.id,  '∞︙تم تعطيل امر منو ضافني', 'md') 
end
--- End Function ---
end
return {Meta = AddedMe}
